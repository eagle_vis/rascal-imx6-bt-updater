// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// Copyright (c) 2016, Laird                                                  ++
//                                                                            ++
// Permission to use, copy, modify, and/or distribute this software for any   ++
// purpose with or without fee is hereby granted, provided that the above     ++
// copyright notice and this permission notice appear in all copies.          ++
//                                                                            ++
// THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES   ++
// WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF           ++
// MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR    ++
// ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES     ++
// WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN      ++
// ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR ++
// IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.                ++
//                                                                            ++
// SPDX-License-Identifier:ISC                                                ++
//                                                                            ++
// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//                                                                            ++
// Source to embed firmware upgrader functionality into a host system which   ++
// interacts with specific Laird modules. The host can be a microcontroller   ++
// or full blown OS based PC like Windows/Linex/Mac or other.                 ++
//                                                                            ++
// The source requires a C++ compiler and has been used to create utilities   ++
// supplied by Laird.                                                         ++
//                                                                            ++
// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

/******************************************************************************/
// CONDITIONAL COMPILE DEFINES
/******************************************************************************/

/******************************************************************************/
// Include Files
/******************************************************************************/
#include "Target.h"
#include "TSerialPortTarget.h"
#include "AtConfig.h"
#include "common.h"


/******************************************************************************/
// Local Defines
/******************************************************************************/

/******************************************************************************/
// Local Macros
/******************************************************************************/

/******************************************************************************/
// Local Forward Class,Struct & Union Declarations
/******************************************************************************/

/******************************************************************************/
// Local Class,Struct,Union Typedefs
/******************************************************************************/

/******************************************************************************/
// External Variable Declarations
/******************************************************************************/

/******************************************************************************/
// Global/Static Variable Declarations
/******************************************************************************/

/******************************************************************************/
// External Function Declarations
/******************************************************************************/

/******************************************************************************/
// Local Forward Function Declarations
/******************************************************************************/

/******************************************************************************/
/******************************************************************************/
// Local Functions or Private Members
/******************************************************************************/
/******************************************************************************/

//=============================================================================
//=============================================================================

/******************************************************************************/
/******************************************************************************/
// Global Functions or Public Members
/******************************************************************************/
/******************************************************************************/

//------------------------------------------------------------------------------
// Constructor
//------------------------------------------------------------------------------
TSerialPortTarget::TSerialPortTarget(TSerialOnRxData *pOwner)
        : TSerialPortBase(pOwner) {
}

//------------------------------------------------------------------------------
// Destructor
//------------------------------------------------------------------------------
TSerialPortTarget::~TSerialPortTarget() {

}

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wunused-parameter"
//------------------------------------------------------------------------------
// This function is called to open the uart interface which provides serial
// access to the Laird module.
//------------------------------------------------------------------------------
bool /* true if the serial port was successfully opened */
TSerialPortTarget::ComOpen(
        char *pDevName, /* specified via #define SERIALPORT_DEVICE_NAME in target.h */
        int nBaudrate,
        int nParity,    /* 0=None, 1=Odd, 2=Even */
        int nStopBits,  /* always 1 */
        int nDataBits   /* always 8 */
) {
    BOOST_LOG_TRIVIAL(error) << "TSerialPortTarget::ComOpen baudrate " << nBaudrate << " parity " << nParity;
    return updater->btOpen();
}
#pragma clang diagnostic pop

//=============================================================================
// This function returns true if the serial port is open via a call of ComOpen()
//=============================================================================
bool TSerialPortTarget::IsOpen() { /* true if the serial port is already open */
    BOOST_LOG_TRIVIAL(error) << "TSerialPortTarget::IsOpen called";
    return updater->fd_ >= 0;
}

//=============================================================================
// Laird modules are designed to go into deep sleep when a uart_break is
// detected. When that uart_break is removed, the module will resume operation
// by resetting.
// This means a break/unbreak is a convenient method of resetting the module.
// If your system's uart is not capable of asserting a break on it's uart_tx line
// then if the module's reset line is driven by your host, then it is acceptable
// to assert a RESET when this function is called.
//=============================================================================
void
TSerialPortTarget::SetBreak() {
    std::string path = updater->gpioBasePath + "bt_state";
    std::string value = "0";

    BOOST_LOG_TRIVIAL(trace) << "SetBreak called";
    if(!updater->writeToFile(path, value)) {
        BOOST_LOG_TRIVIAL(error) << "Could not reset BT module: " << path;
        return;
    }
}

//=============================================================================
// Laird modules are designed to go into deep sleep when a uart_break is
// detected. When that uart_break is removed, the module will resume operation
// by resetting.
// This means a break/unbreak is a convenient method of resetting the module.
// If your system's uart is not capable of asserting a break on it's uart_tx line
// then if the module's reset line is driven by your host, then it is acceptable
// to deassert the RESET when this function is called.
//=============================================================================
void
TSerialPortTarget::ClearBreak() {
    std::string path = updater->gpioBasePath + "bt_state";
    std::string value = "1";

    BOOST_LOG_TRIVIAL(trace) << "ClearBreak called";

    if(!updater->writeToFile(path, value)) {
        BOOST_LOG_TRIVIAL(error) << "Could not reset BT module: " << path;
        return;
    }
}

//=============================================================================
// Laird modules with smartBASIC have an nAutorun pin which is deasserted (taken
// high) then an autorunning smartBASIC application is supressed on power up.
// Hence as part of the firmware upgrade the DTR line which on Laird Devkits is
// connected to the nAutorun will be deasserted at the start of the process.
//
// If in your system your host has a gpio line which is driving the nAutorun
// pin of the module, then calling this function should set the nAutorun pin
// to high
//=============================================================================
void
TSerialPortTarget::DeassertDTR() {
    BOOST_LOG_TRIVIAL(trace) << "DeassertDTR called";
}

//=============================================================================
// Laird modules with smartBASIC have an nAutorun pin which is deasserted (taken
// high) then an autorunning smartBASIC application is supressed on power up.
// Hence as part of the firmware upgrade the DTR line which on Laird Devkits is
// connected to the nAutorun will be deasserted at the start of the process.
//
// If in your system your host has a gpio line which is driving the nAutorun
// pin of the module, then calling this function should set the nAutorun pin
// to low
//=============================================================================
void
TSerialPortTarget::AssertDTR(
) {
    BOOST_LOG_TRIVIAL(trace) << "AssertDTR called";
}

//=============================================================================
// This function returns the current state of the RTS output of the Laird module
// which will be connected to the CTS input of the host mcu
//=============================================================================
bool  /* true if asserted */
TSerialPortTarget::GetCtsState() {
    return true;
}

//=============================================================================
// This function will be called by the engine to flush the low level receive
// buffers
//=============================================================================
void
TSerialPortTarget::FlushRxBuffer() {
    updater->btFlushRx();
}

//=============================================================================
// This function will be called by the engine to flush the low level transmit
// buffers
//=============================================================================
void
TSerialPortTarget::FlushTxBuffer() {
    updater->btFlushTx();
}

//=============================================================================
// This function will be called to output the buffer specified to the Laird module
// and the parameter nLen specifies how many bytes in that buffer to send.
//=============================================================================
unsigned /* number of bytes actually sent */
TSerialPortTarget::PutBlock(
        unsigned char *pTxBlock,  /* pointer to data that needs to be sent */
        unsigned short nLen       /* number of bytes to send */
) {
    return updater->btWrite(pTxBlock, nLen);
}

//=============================================================================
// This function is used to call a maximum of nCount bytes from the low level
// buffer and the function will return the actual number returned.
//=============================================================================
unsigned /* number of bytes actually copied to destination buffer */
TSerialPortTarget::GetBlock(
        char *pDest,  /* pointer to buffer where data will be copied */
        unsigned nCount /* maximum bytes to copy to buffer */
) {
    return updater->btRead((uint8_t *)pDest, nCount);
}

//=============================================================================
// This function will be called to close the serial interface and no further
// serial activity will be allowed apart from ComOpen() and IsOpen()
//=============================================================================
void TSerialPortTarget::Close() {
    updater->btClose();
}

/******************************************************************************/
/******************************************************************************/
// Global Functions Non Class Members
/******************************************************************************/
/******************************************************************************/

/*============================================================================*/
/* This function returns the number of receive bytes waiting in an            */
/* underlying buffer waiting to be read                                       */
/* If need be take care of any threading issues                               */
/*============================================================================*/
unsigned int  /* returns the number of bytes waiting in the low level rx buffer */
SerialRxDataCount() {
    return updater->btBytesAvailable();
}

/******************************************************************************/
// END OF FILE
/******************************************************************************/
