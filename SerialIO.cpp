//
// Created by Juliusz Hoffman on 06.06.19.
//

#pragma ide diagnostic ignored "hicpp-signed-bitwise"

#include <termios.h>
#include <termio.h>
#include <fcntl.h>
#include "SerialIO.h"
#include "common.h"

bool SerialIO::openPort() {
    struct termios tty{0};
    int speed;

    BOOST_LOG_TRIVIAL(debug) << "SerialIO[" << port_ << "]: Opening port with " << speed_;

    switch (speed_) {
        case 4800:
            speed = B4800;
            break;
        case 9600:
            speed = B9600;
            break;
        case 19200:
            speed = B19200;
            break;
        case 38400:
            speed = B38400;
            break;
        case 57600:
            speed = B57600;
            break;
        case 115200:
            speed = B115200;
            break;
        default:
            BOOST_LOG_TRIVIAL(error) << "SerialIO[" << port_ << "]: Invalid port speed " << speed_;
            return false;
    }

    fd_ = open(port_.c_str(), (O_RDWR | O_NDELAY | O_NOCTTY));
    if (fd_ < 0) {
        BOOST_LOG_TRIVIAL(error) << "SerialIO[" << port_ << "]: could not open port: " << fd_ << " - "
                                 << std::strerror(errno);;
        return false;
    }

    tcgetattr(fd_, &tty);
    cfsetospeed(&tty, speed);
    cfsetispeed(&tty, speed);

    cfmakeraw(&tty);

    tty.c_cflag &= ~PARENB;   /* Disables the Parity Enable bit(PARENB),So No Parity   */
    tty.c_cflag &= ~CSTOPB;   /* CSTOPB = 2 Stop bits,here it is cleared so 1 Stop bit */
    tty.c_cflag &= ~CSIZE;    /* Clears the mask for setting the data size             */
    tty.c_cflag |= CS8;       /* Set the data bits = 8                                 */

    tty.c_cflag &= ~CRTSCTS;       /* No Hardware flow Control                         */
    tty.c_cflag |= CREAD | CLOCAL; /* Enable receiver,Ignore Modem Control lines       */


    tty.c_iflag &= ~(IXON | IXOFF | IXANY);          /* Disable XON/XOFF flow control both i/p and o/p */
    tty.c_iflag &= ~(ICANON | ECHO | ECHOE | ISIG);  /* Non Cannonical mode                            */
    tty.c_iflag &= ~(IGNCR | ICRNL);                 /* Leave Carriage Return as is                    */

    tty.c_oflag &= ~OPOST;                           /*No Output Processing*/

    if ((tcsetattr(fd_, TCSANOW, &tty)) != 0) { /* Set the attributes to the termios structure*/
        BOOST_LOG_TRIVIAL(error) << "SerialIO[" << port_ << "]: Error " << errno << " from tcsetattr";
        closePort();
        return false;
    }

    return true;
}

void SerialIO::closePort() {
    if (fd_ < 0) {
        return;
    }
    BOOST_LOG_TRIVIAL(debug) << "SerialIO[" << port_ << "]: closing port";
    close(fd_);
    fd_ = -1;
}

void SerialIO::run() {
    fd_set set;
    struct timeval timeout{0};
    int select_ret, num_read;
    uint8_t buffer[256];

    pipeInit();
    openPort();

    while (running_) {

        if (fd_ >= 0) {
            FD_ZERO (&set);
            FD_SET (fd_, &set);
            FD_SET(pipe_fds_[0], &set);
            timeout.tv_sec = SERIAL_READ_DELAY_SEC;
            timeout.tv_usec = SERIAL_READ_DELAY_USEC;
            select_ret = select(FD_SETSIZE, &set, NULL, NULL, &timeout); // NOLINT(modernize-use-nullptr)
        } else {
            select_ret = -1;
        }

        BOOST_LOG_TRIVIAL(trace) << "SerialIO[" << port_ << "]: select() returned " << select_ret;
        if (select_ret < 0) {
            BOOST_LOG_TRIVIAL(error) << "SerialIO[" << port_ << "]: select() returned " << select_ret;
            // do not automatically reopen port
            if (!autoReopen_) {
                closePort();
                while (!autoReopen_ && running_) {
                    std::unique_lock<std::mutex> lock_(exit_guard_);
                    exit_condition_.wait_for(lock_, std::chrono::seconds(1));
                }
                if (!running_) {
                    break;
                }
            }
            do {
                closePort();
                {
                    std::unique_lock<std::mutex> lock_(exit_guard_);
                    exit_condition_.wait_for(lock_, std::chrono::seconds(1));
                }

                if (!running_) {
                    break;
                }
                BOOST_LOG_TRIVIAL(error) << "SerialIO[" << port_ << "]: reopening port";
            } while (running_ && !openPort());
        } else if (select_ret) {
            if (FD_ISSET(pipe_fds_[0], &set)) { // exit pipe was called
                BOOST_LOG_TRIVIAL(info) << "SerialIO[" << port_ << "]: exit called";
                pipeClose();
                closePort();
                break;
            } else {
                num_read = read(fd_, &buffer, 256);
                try {
                    if (num_read > 0) {
                        buffer[num_read+1] = 0;
#ifdef LOG_TRACE_ENABLED
                        std::stringstream ss;
                        ss << "SerialIO[" << port_ << "]: read " << num_read << " bytes: ";
                        for(int i=0; i < num_read; ++i) {
                            ss << " 0x" << std::hex << static_cast<int>(buffer[i]);
                        }
                        LOG_TRACE(ss.str())
#endif
                        onData(num_read, buffer);
                    } else {
                        closePort();
                    }
                } catch (const std::exception &e) {
                    BOOST_LOG_TRIVIAL(error) << "SerialIO[" << port_ << "]: " << e.what();
                }
            }
        } // else timeout, retry
    }
    closePort();
}

bool SerialIO::transmit(const uint8_t *data, int num_bytes) {
    LOG_TRACE("SerialIO[" << port_ << "]: TX " << num_bytes << " bytes");
    if (fd_ >= 0) {
        int ret = ::write(fd_, data, num_bytes);
        if (ret == num_bytes) {
            tcdrain(fd_);
            return true;
        } else {
            BOOST_LOG_TRIVIAL(error) << "SerialIO[" << port_ << "]: TX failed";
        }
    } else {
        BOOST_LOG_TRIVIAL(error) << "SerialIO[" << port_ << "]: TX - FD not open";
    }
    return false;
}

bool SerialIO::pipeInit() {
    int flags;
    if (pipe(pipe_fds_) == -1) {
        BOOST_LOG_TRIVIAL(error) << "SerialIO[" << port_ << "]: pipe() failed";
        return false;
    }

    flags = fcntl(pipe_fds_[0], F_GETFL);
    if (flags == -1) {
        BOOST_LOG_TRIVIAL(error) << "SerialIO[" << port_ << "]: fcntl(F_GETFL) failed";
        return false;
    }
    flags |= O_NONBLOCK;                /* Make read end nonblocking */
    if (fcntl(pipe_fds_[0], F_SETFL, flags) == -1) {
        BOOST_LOG_TRIVIAL(error) << "SerialIO[" << port_ << "]: fcntl(F_SETFL) failed";
        return false;
    }

    flags = fcntl(pipe_fds_[1], F_GETFL);
    if (flags == -1) {
        BOOST_LOG_TRIVIAL(error) << "SerialIO[" << port_ << "]: fcntl(F_GETFL) failed";
        return false;
    }
    flags |= O_NONBLOCK;                /* Make write end nonblocking */
    if (fcntl(pipe_fds_[1], F_SETFL, flags) == -1) {
        BOOST_LOG_TRIVIAL(error) << "SerialIO[" << port_ << "]: fcntl(F_SETFL) failed";
        return false;
    }
}

void SerialIO::pipeClose() {
    close(pipe_fds_[0]);
    close(pipe_fds_[1]);
}

void SerialIO::closeThread() {
    ThreadBase::closeThread();
    setAutomaticPortReopen(false);
    closePort();
    write(pipe_fds_[1], "x", 1);
    exit_condition_.notify_all();
}
