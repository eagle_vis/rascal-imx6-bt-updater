#include <iostream>
#include "Target.h"

#include <stdio.h>
#include "UwFlashEmbed.h"
#include "Misc.h"

#include <boost/filesystem.hpp>
#include <boost/program_options.hpp>
#include <boost/log/core.hpp>
#include <boost/log/expressions.hpp>
#include <boost/log/utility/setup/console.hpp>
#include <boost/log/utility/setup/common_attributes.hpp>
#include <boost/date_time/posix_time/posix_time_types.hpp>
#include <boost/log/support/date_time.hpp>
#include <boost/exception/all.hpp>
#include <termios.h>
#include <UpdaterData.h>
#include <thread>

#include "common.h"

std::shared_ptr<UpdaterData> updater;

int main(int argc, char **argv) {

    std::string filePath;
    std::string gpioBasePath;
    std::string bt_port_data;
    boost::log::trivial::severity_level log_level;

    try {
        namespace po = boost::program_options;

        po::options_description desc("Options");
        desc.add_options()
                ("help,h", "Print help messages")
                ("path,p", po::value<std::string>(&filePath)->default_value("/usr/share/rascal/BL652.uwf"), "Upgrade file")
                ("bt,b", po::value<std::string>(&bt_port_data)->default_value("/dev/ttymxc2:115200"), "BlueTooth port")
                ("gpio,g", po::value<std::string>(&gpioBasePath)->default_value("/sys/kernel/rascal_gpio/"))
                ("log_level,d", boost::program_options::value<boost::log::trivial::severity_level>(&log_level)
                        ->default_value(boost::log::trivial::severity_level::warning), "log level to output");
        po::variables_map vm;
        try {
            po::store(po::parse_command_line(argc, argv, desc), vm); // can throw
            if (vm.count("help")) {
                std::cout << "Basic Command Line Parameter App" << std::endl
                          << desc << std::endl;
                return 0;
            }

            po::notify(vm);
        }
        catch (po::error &e) {
            std::cerr << "ERROR: " << e.what() << std::endl << std::endl;
            std::cerr << desc << std::endl;
            return -1;
        }
    }
    catch (std::exception &e) {
        std::cerr << "Unhandled Exception reached the top of main: "
                  << e.what() << ", application will now exit" << std::endl;
        return -1;
    }

    std::set_terminate([]() {
        std::exception_ptr eptr = std::current_exception();
        try {
            if (eptr) {
                std::rethrow_exception(eptr);
            }
        }
        catch (const boost::exception &e) {
            std::cerr << "Unhandled exception \"" << boost::diagnostic_information(e) << "\"" << std::endl;
        }
        catch (const std::exception &e) {
            std::cerr << "Unhandled exception \"" << e.what() << "\"" << std::endl;
        }
    });

    boost::log::core::get()->set_filter(boost::log::trivial::severity >= log_level);
    boost::log::add_console_log(std::cout, boost::log::keywords::format = boost::log::expressions::stream
            << "[BtUpdater]"
            << boost::log::expressions::format_date_time<boost::posix_time::ptime>("TimeStamp", "[%H:%M:%S.%f]")
            << "[" << std::setw(7) << std::setfill(' ') << boost::log::trivial::severity
            << "] " << boost::log::expressions::smessage);
    boost::log::add_common_attributes();

    std::shared_ptr<std::thread> btIOThread;

    try {
        size_t separator_pos = bt_port_data.rfind(':', bt_port_data.length());
        if (separator_pos == std::string::npos) {
            throw std::invalid_argument("Invalid port data");
        }
        std::string btPort = bt_port_data.substr(0, separator_pos);
        std::string btSpeed = bt_port_data.substr(separator_pos + 1, std::string::npos);
        if(!boost::filesystem::exists(filePath)) {
            BOOST_LOG_TRIVIAL(error) << "File \"" << filePath << "\" does not exist";
            return -1;
        }

        updater = std::make_shared<UpdaterData>(btPort, btSpeed);

        updater->filePath = filePath;
        updater->gpioBasePath = gpioBasePath;
        updater->running_ = true;
        if (updater->gpioBasePath.back() != '/') {
            updater->gpioBasePath += '/';
        }

        btIOThread = std::make_shared<std::thread>(std::bind(&UpdaterData::run, updater));

    } catch (const std::exception &e) {
        BOOST_LOG_TRIVIAL(error) << "Exception encountered while parsing program options data: " << e.what();
        return -1;
    }

    BOOST_LOG_TRIVIAL(info) << "Starting";
    UwFlashUpgradeFirmare();
    if(updater)
        updater->closeThread();
    if(btIOThread)
        btIOThread->join();
    return 0;
}