//
// Created by eaglevis on 15.07.2019.
//

#include "UpdaterData.h"
#include <algorithm>
#include <termios.h>
#include <unistd.h>

unsigned int UpdaterData::btBytesAvailable() {
    if(fd_ < 0) {
        serialPos = 0;
    }
    std::this_thread::yield();
    if(serialPos)
        BOOST_LOG_TRIVIAL(trace) << "btBytesAvailable called with serialPos=" << serialPos;
    return serialPos;

}

int UpdaterData::btWrite(uint8_t *dest, unsigned short count) {
    BOOST_LOG_TRIVIAL(trace) << "btWrite called";
    transmit(dest, count);
    tcdrain(fd_);
    return 0;
}

int UpdaterData::btRead(uint8_t *dest, unsigned short max_num) {
    int count = std::min<int>(serialPos, max_num);
    BOOST_LOG_TRIVIAL(trace) << "btRead called with count=" << count;

    memcpy(dest, buffer, count);
    assert(dest[0] == buffer[0]);

    if(count - serialPos != 0) {
        memcpy(buffer, buffer+count, serialPos - count);
    }

#ifdef LOG_TRACE_ENABLED
    std::stringstream ss;
    ss << "btRead]: data: ";
    for(int i=0; i < count; ++i) {
        ss << " 0x" << std::hex << static_cast<int>(buffer[i]);
    }
    LOG_TRACE(ss.str())
#endif

    serialPos -= count;

    return count;
}

void UpdaterData::onData(int num_read, uint8_t *dataBuffer) {
    memcpy(buffer+serialPos, dataBuffer, num_read);
    assert(*(buffer+serialPos) == dataBuffer[0]);
#ifdef LOG_TRACE_ENABLED
    std::stringstream ss;
    ss << "UpdaterData[" << port_ << "]: read " << num_read << " bytes: ";
    for(int i=0; i < num_read; ++i) {
        ss << " 0x" << std::hex << static_cast<int>(buffer[i]);
    }
    LOG_TRACE(ss.str())
#endif
    serialPos += num_read;
}
