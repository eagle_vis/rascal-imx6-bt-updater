//
// Created by Juliusz Hoffman on 12.06.19.
//

#ifndef RASCAL_BTUPDATER_COMMON_H
#define RASCAL_BTUPDATER_COMMON_H

#pragma ide diagnostic ignored "hicpp-signed-bitwise"


#include "UpdaterData.h"
#include <memory>

extern std::shared_ptr<UpdaterData> updater;
#endif //RASCAL_BTUPDATER_COMMON_H
