//
// Created by Juliusz Hoffman on 06.06.19.
//

#ifndef RASCAL_MAINAPP_SERIALIO_H
#define RASCAL_MAINAPP_SERIALIO_H


#include "ThreadBase.h"
#include <mutex>
#include <condition_variable>

class SerialIO : public ThreadBase {
public:
    std::mutex exit_guard_;
    std::condition_variable exit_condition_;
    int pipe_fds_[2] = {};

    int fd_ = -1;
    std::string port_;
    int speed_;

    bool autoReopen_ = true;

    virtual void onData(int bytes, uint8_t *buffer) = 0;

public:
    SerialIO(std::string port, const std::string &speed)
            : ThreadBase(), port_(std::move(port)), speed_(std::stoi(speed)) {
        running_ = true;
    };
//    ~SerialIO() { closePort(); }

    bool pipeInit();

    void pipeClose();

    bool openPort();

    void closePort();

    void run() override;

    void closeThread() override;

    bool transmit(const char *data, const int num_bytes) { return transmit((uint8_t *) data, num_bytes); };

    bool transmit(const uint8_t *data, int num_bytes);

    void setAutomaticPortReopen(bool state) {
        autoReopen_ = state;
    }
};

#define SERIAL_READ_DELAY_SEC        10
#define SERIAL_READ_DELAY_USEC       0


#endif //RASCAL_MAINAPP_SERIALIO_H
