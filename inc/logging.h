//
// Created by eaglevis on 15.07.2019.
//

#ifndef RASCAL_BTUPDATER_LOGGING_H
#define RASCAL_BTUPDATER_LOGGING_H

#include <boost/log/trivial.hpp>
#define LOG_TRACE_ENABLED

#define LOG_DEBUG_ENABLED
#define LOG_INFO_ENABLED

#ifdef LOG_TRACE_ENABLED
#define LOG_TRACE(what) \
  BOOST_LOG_TRIVIAL(trace) << what;
#else
#define LOG_TRACE(x)
#endif

#ifdef LOG_DEBUG_ENABLED
#define LOG_DEBUG(what) \
  BOOST_LOG_TRIVIAL(debug) << what
#else
#define LOG_DEBUG(x)
#endif

#ifdef LOG_INFO_ENABLED
#define LOG_INFO(what) \
  BOOST_LOG_TRIVIAL(info) << what
#else
#define LOG_DEBUG(x)
#endif

#define LOG_WARNING(what) \
    BOOST_LOG_TRIVIAL(warning) << what

#define LOG_ERROR(what) \
    BOOST_LOG_TRIVIAL(error) << what

#define LOG_fatal(what) \
    BOOST_LOG_TRIVIAL(fatal) << what
#endif //RASCAL_BTUPDATER_LOGGING_H
