//
// Created by Juliusz Hoffman on 23.05.19.
//

#ifndef RASCAL_MAINAPP_THREADBASE_H
#define RASCAL_MAINAPP_THREADBASE_H

#include <atomic>
#include <thread>
#include "logging.h"
class ThreadBase {
public:
    std::atomic_bool running_;

    virtual void closeThread() {
        running_ = false;
    };

    virtual void run()  {
        BOOST_LOG_TRIVIAL(fatal) << "ThreadBase::run should never be called";
        throw std::runtime_error("ThreadBase::run should never be called");
    };

};

#endif //RASCAL_MAINAPP_THREADBASE_H
