//
// Created by eaglevis on 15.07.2019.
//

#ifndef RASCAL_BTUPDATER_UPDATERDATA_H
#define RASCAL_BTUPDATER_UPDATERDATA_H


#include <string>
#include <fstream>

#include <sys/ioctl.h>

#include <termios.h>
#include <termio.h>
#include <fcntl.h>
#include <fstream>
#include <chrono>
#include <cstdio>
#include <iostream>
#include <sstream>

#include "SerialIO.h"

class UpdaterData : public SerialIO {
public:
    std::string filePath;
    FILE * fileFd = nullptr;
    std::string gpioBasePath;


    unsigned int progress_min=0;
    unsigned int progress_max=100;

    std::chrono::steady_clock::time_point timePointStart;

    int serialPos = 0;
    uint8_t buffer[1024] = {};

    UpdaterData(std::string port, const std::string& speed) : SerialIO(std::move(port), speed), timePointStart(std::chrono::steady_clock::now()) {};

    static bool writeToFile(std::string &path, std::string &value) {
        std::ofstream f(path);
        if (f) {
            f << value;
            f.flush();
            return f.good();
        }
        return false;
    }

    unsigned int btBytesAvailable();

    bool btOpen() {
        BOOST_LOG_TRIVIAL(trace) << "btCpen called";
        return true;
    }

    void btClose(){
        BOOST_LOG_TRIVIAL(trace) << "btClose called";
        return;
        close(fd_);
        fd_ = -1;
    }

    int btRead(uint8_t* dest, unsigned short max_num);

    int btWrite(uint8_t* dest, unsigned short count) ;

    void btFlushRx() {
        BOOST_LOG_TRIVIAL(trace) << "btFlushRx called";
        tcflush(fd_, TCIFLUSH);
        serialPos = 0;
    }

    void btFlushTx() {
        BOOST_LOG_TRIVIAL(trace) << "btFlushTx called";
        tcflush(fd_, TCOFLUSH);
    }

    unsigned int millisSinceStart() {
        auto now = std::chrono::steady_clock::now();
        return std::chrono::duration_cast<std::chrono::milliseconds>(now - timePointStart).count();
    }

    void* updateOpen() {
        BOOST_LOG_TRIVIAL(trace) << "updateOpen called";
        fileFd = fopen(filePath.c_str(), "rb");
        if(fileFd == nullptr) {
            BOOST_LOG_TRIVIAL(error) << "Cannot open \"" << filePath << "\"";
            exit(-1);
        }
        return &fileFd;
    }

    int fileClose(){
        BOOST_LOG_TRIVIAL(trace) << "fileClose called";
        int ret = fclose(fileFd);
        fileFd = nullptr;
        return ret;
    }

    size_t fileRead(void* ptr, size_t size, size_t count) {
//        BOOST_LOG_TRIVIAL(trace) << "fileRead called " << size << " - " << count;
        return fread(ptr, size, count, fileFd);
    }

    void onData(int num_read, uint8_t *buffer) override;

};

#endif //RASCAL_BTUPDATER_UPDATERDATA_H
